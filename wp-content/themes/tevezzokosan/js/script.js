var $ = jQuery;
$(document).ready(function() {
	
	jQuery('#main_menu').append('<span class="menu_trigger">Menu</span>');
	jQuery('.menu_trigger').click(function() {
		jQuery('body').toggleClass('open');
	});
	
		
        function onNavigationClick(){
            var queryDate = $(this).attr('data-date');
            console.log(queryDate);
            $.ajax({
                url: "/getsuggestion",
                method: 'post',
                data: {
                    queryDate: queryDate
                },
                success: function(response){
                    //console.log(response);
                    //console.log($('#suggestions'));
                    $('#suggestions').remove();
                    $(response).insertAfter('div.nivo-controlNav');
                    $('.navigateRight, .navigateLeft').click(onNavigationClick);
                    $('.tabHead').click(onTabHeadClick);
                }
            });
        }
        
        $('.navigateRight, .navigateLeft').click(onNavigationClick);
        
        $('.tabHead').click(onTabHeadClick);
        
        function onTabHeadClick(){
            $('.tabHead').removeClass('active');
            $(this).addClass('active');
            var elementIndex = $(this).index() - 1;
            $('.tabContent').css({display: 'none'});
            $('.tabContent:eq(' + elementIndex + ')').css({display: 'block'});
        }
        
        $('#nivoslider').nivoSlider({
			effect:"fade",
			slices:1,
			boxCols:1,
			boxRows:1,
			animSpeed:333,
			pauseTime: 3400,
			//startSlide:0,
			//directionNav:true,
			//directionNavHide:true,
			//controlNav:true,
			//controlNavThumbs:false,
			//controlNavThumbsFromRel:true,
			//keyboardNav:true,
			//pauseOnHover:true,
			//manualAdvance:true,
			//captionOpacity: 0.8,
			//prevText: 'Előző',
			//nextText: 'Következő'
			/* EFFECTS: sliceDown sliceDownLeft sliceUp sliceUpLeft sliceUpDown sliceUpDownLeft fold fade random slideInRight slideInLeft boxRandom boxRain boxRainReverse boxRainGrow boxRainGrowReverse */
		});
        
});
