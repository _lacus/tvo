
<div class="slider-wrapper theme-<?php echo($theme); ?> place">
	<div class="ribbon"></div>
	<div id="nivoslider" class="nivoSlider szlogen">
		<?php query_posts(array('post_type' => 'szlogen', 'orderby'=>'date','order'=>'DESC','orderby' => 'rand', 'posts_per_page'=>33));
			$cnt = 0;
			while ( have_posts() ) : the_post(); ?>
				<?php global $post; $post->url = get_post_meta($post->ID, 'url', true); ?>
					<?php if ( has_post_thumbnail() ) { ?>
						<?php if(!empty($post->url)){ ?>
							<a href="<?php echo $post->url ?>" rel="bookmark">
						<?php } ?>
							<?php the_post_thumbnail(array(700,500), array("title"=>"#slider-caption-".get_the_ID())); ?>
						<?php if(!empty($post->url)){ ?>
							</a>
						<?php } ?>
				<?php } ?>
				<!--h3><a href="<?php echo get_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a></h3>-->
				
				<?php
					//the_excerpt_rereloaded('10','Olvass tovább','','div','button middle gray','no')
					$slider_part .= '
						<div id="slider-caption-'.get_the_ID().'" class="nivo-html-caption">
							<h2>
                                                        ' . (!empty($post->url) ? '<a href="'.$post->url.'">' : '') . ''
                                                        .get_the_title().'
                                                        ' . (!empty($post->url) ? '</a>' : '') . '
                                                        </h2>
                                                        ' . (!empty($post->url) ? '<a href="'.$post->url.'">' : '') . ''
								.the_excerpt_rereloaded('1500','Olvass tovább','','div','button middle gray','no', false, true).'
							' . (!empty($post->url) ? '</a>' : '') . '
						</div>
					';
					$cnt++;
				?>
			<?php endwhile;
		wp_reset_query(); ?>
	</div>
</div>
<?php echo $slider_part; ?>
