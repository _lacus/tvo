<div id="nivoslider" class="nivoSlider textbreak">
            <?php
                $todaysRecommendations = getPostsOnDateByCategoryId(strtotime('now + 0 day'), 483); //'now'
                $tomorrowRecommendations = getPostsOnDateByCategoryId(strtotime('now + 1 day'), 483); //'now + 1 day'
                $aftertomorrowRecommendations = getPostsOnDateByCategoryId(strtotime('now + 2 day'), 483); //'now'
                $freetimePrograms = getFuturePostsByCategoryId(5);
                ////////////////
                //looping today
                ////////////////
                while($todaysRecommendations->have_posts()){
                    $todaysRecommendations->next_post();
                    ?>
                        <a href="<?php echo get_permalink($todaysRecommendations->post->ID); ?>" class="img" title="#<?php echo 'caption-' . $todaysRecommendations->post->ID; ?>" rel="bookmark">
                            <img title="#<?php echo 'caption-' . $todaysRecommendations->post->ID; ?>" src="<?php $srcArr = wp_get_attachment_image_src( get_post_thumbnail_id($todaysRecommendations->post->ID), 'main-slider' ); echo $srcArr[0];?>" alt="<?php echo $todaysRecommendations->post->post_title;?>" />
                        </a>
                    <?php
                } ?>
</div>

<?php
////////////////
//looping today
////////////////
while($todaysRecommendations->have_posts()){
    $todaysRecommendations->next_post();
    ?>
        <div class="hidden_from_eyes" id="<?php echo 'caption-' . $todaysRecommendations->post->ID; ?>">
            <h3>
                <?php echo $todaysRecommendations->post->post_title; ?>
            </h3>
            <div class="meta">
                <strong><?php echo $todaysRecommendations->post->channel;?></strong>
                <span class="datetime">
                    <?php echo date('Y. m. d.', strtotime($todaysRecommendations->post->post_date));?>
                </span><br />
                <span class="datetime">
                    <?php echo date('H:i', strtotime($todaysRecommendations->post->post_date));?>
                </span>
                <span>
                    <?php // echo $todaysRecommendations->post->customCategory?>
                </span>
            </div>
        </div>
    <?php
}
