<div id="suggestions">
	<h3>Gyermekműsor-ajánló</h3>
	<div class="tabContainer">
		<?php
		    $queryDate = 'now';
		    $queryDate = Date('Y-m-d', strtotime('last monday', strtotime($queryDate)));
                    
                    
                    if(date('D', strtotime('now')) === 'Mon'){
                        $queryDate = Date('Y-m-d', strtotime('now'));
                    }
                    
                    
		    $classHelper = strtotime($_REQUEST['queryDate']) - strtotime($queryDate);
		    
		    $activeClass = null;
		    
		    if($classHelper === 604800){
		        $activeClass = true;
		    }
		    
		    if($classHelper === -604800){
		        $activeClass = false;
		    }
		    
		    if(!empty($_GET['queryDate'])){
		        $queryDate = $_GET['queryDate'];
		    }

		    if(!empty($_POST['queryDate'])){
		        $queryDate = $_POST['queryDate'];
		    }
		    echo '<span class="hidden_from_eyes">'.$queryDate.'</span>';
		?>
		<div class="tabHeads">
		    <div class="navigateLeft<?php echo ($activeClass === false) ? ' disabled' : ''; ?>" data-date="<?php echo date('Y-m-d', strtotime('last monday', strtotime($queryDate))); ?>">
		    </div>
		    <?php
                    
		        $daysOfThisWeek = getDaysOfThisWeek($queryDate);
                        
                        $dw = date("w", strtotime('now')) - 1;
                        if($dw === -1)
                            $dw = 6;
                        
                        
		        foreach ($daysOfThisWeek as $key => $dayOfThisWeek) {
                            
		            ?>
		                <div class="tabHead<?php echo ($key == $dw) ? ' active' : '';?>">
		                    <?php echo  $napok[date('w', $dayOfThisWeek)] . '<br /><span>' . $honap[date('n', $dayOfThisWeek)] . ' ' . date('d', $dayOfThisWeek) . '</span>'; ?>
		                </div>
		    <?php
		        }
		    ?>
		    <div class="navigateRight<?php echo ($activeClass === true) ? ' disabled' : ''; ?>" data-date="<?php echo date('Y-m-d', strtotime('next monday', strtotime($queryDate))); ?>">
		    </div>    
		</div>
		<div class="tabContents">
		    <?php
		         foreach ($daysOfThisWeek as $key => $dayOfThisWeek) {
		            ?>
		            <div class="tabContent" style="<?php echo ($key == $dw) ? 'display:block;' : 'display:none;';?>">

		            <?php

		                $postsOnSelectedDateQuery = getPostsOnDateByCategoryId($dayOfThisWeek, 483);
		                if($postsOnSelectedDateQuery->post_count > 0){
		                while($postsOnSelectedDateQuery->have_posts()){
		                    $postsOnSelectedDateQuery->next_post();
		                    if($postsOnSelectedDateQuery->post->ID !== false){
		                    $post = $postsOnSelectedDateQuery->post;
		                    $image = wp_get_attachment_image( get_post_thumbnail_id($postsOnSelectedDateQuery->post->ID), 'suggested-thumb3');
		                    $permalink = get_permalink($postsOnSelectedDateQuery->post->ID);
		            ?>
		                    <div class="recommend">
		                        <span class="date"><?php echo date('H:i', strtotime($postsOnSelectedDateQuery->post->post_date)); ?></span>
		                        
		                        <?php if($image){ ?>
		                            <a href="<?php echo $permalink; ?>" title="<?php echo $postsOnSelectedDateQuery->post->post_title;?>">
		                                <span class="img">
		                                    <?php echo $image; ?>
		                                </span>
		                            </a>
		                        <?php } ?>
		                            <h3>
		                                <a href="<?php echo $permalink; ?>" title="<?php echo $postsOnSelectedDateQuery->post->post_title;?>"><?php echo $postsOnSelectedDateQuery->post->post_title;?></a>
		                            </h3>
		                      
		                        
		                        <?php // echo getFirstWords($postsOnSelectedDateQuery->post->post_content, 36);?>
		                        <?php the_excerpt_rereloaded($words = 32, $link_text = ''); ?>
		                        <a class="more" href="<?php echo $permalink; ?>" title="<?php echo $postsOnSelectedDateQuery->post->post_title;?>">Tovább</a>
		                        <span class="meta"><?php echo $postsOnSelectedDateQuery->post->channel;?> - <?php echo $postsOnSelectedDateQuery->post->customCategory?></span>
				                <div class="tags icon_tags">
		                            <?php the_tags('',''); ?>
		                        </div>
		                        <?php if(get_field('szuloknek')) { ?>
									<a class="parent_info" href="<?php echo $permalink; ?>/#szuloknek">Szülőknek</a>
								<?php } ?>
		                    </div>
		            <?php
		                }
		                }
		                }else{
		                    ?>
		                    <p>Erre a napra jelenleg nincsenek programok feltöltve.</p>
		                    <?php
		                }
		            ?>

		        </div>
		    <?php
		        }
		    ?>
		</div>
	</div>
	<div class="icon_key">
		
		<?php $recent = new WP_Query("page_id=26840");
		while ($recent->have_posts()) : $recent->the_post(); ?>
			<h3><?php the_title(); ?></h3>
			<a class="info" href="/piktogramokrol-reszletesen">Piktogramokról részletesen</a>
			<?php the_content();
		endwhile;
		wp_reset_postdata(); ?>
	</div>
</div>
