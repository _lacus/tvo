<h2 style="margin-bottom: 3px;">TIPP</h2>
<?php
	$args = array( 'post_type' => 'szlogen', 'orderby' => 'rand', 'posts_per_page' => 1  );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post(); ?>
		<a class="sidebar_slogen" href="/szlogen">
			<?php the_post_thumbnail(array(300,300)); ?>
			<span><?php the_title(); ?></span>
		</a>
	<?php endwhile;
?>
