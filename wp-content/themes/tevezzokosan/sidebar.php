<aside>
	<?php if(is_front_page() || is_page(12217) || in_category(array(483, 5, 1408, 1375, 1434, 1438))) { ?>
    <section>
    	<?php include_once 'components/sidebar_slogen.php'; ?>
    	
    	<a href="/videok/gyerekzar-1" class="sidebar-video">
    		<span class="img"><img src="<?php bloginfo('template_directory'); ?>/img/video.png" /></span>
    		<span>Gyerekzár</span>
    	</a>
    	<?php if(is_category(5)) { ?>
        <?php } else { ?>
        
        <h2>Szabadidős programajánló</h2>
        <div>
            <?php
            if(is_front_page()) {
            	$sidebar_front_nmb = 3;
            } else {
            	$sidebar_front_nmb = 3;
            }
            
            function filter_where($where = '') {
                //$where .= " AND post_date >= '" . date('Y-m-d') . "' ";
                $where .= " AND post_date >= '2014-03-12' ";
                return $where;
            }
            
            //add_filter('posts_where', 'filter_where');
            function wpse52070_filter_where($where = '', $query) {
                // posts in the last 30 days
                $where .= " AND post_date >= '" . date('Y-m-d', strtotime('now')) . "'";
                return $where;
            }
            
            add_filter('posts_where', 'wpse52070_filter_where');
            
            query_posts(array('posts_per_page' => $sidebar_front_nmb, 'cat' => 5, 'orderby' => 'date', 'order' => 'asc'));
            
            while (have_posts()) : the_post();
                global $post;
                
                ?>
                <div class="article">
                    <?php $title = get_the_title(); ?>
                    <h3 class="title"><a href="<?php echo get_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a></h3>
                    <?php if (has_post_thumbnail()) { ?>
                        <a href="<?php echo get_permalink() ?>" class="img" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_post_thumbnail(array(150, 150)); ?></a>
                    <?php } else { ?>
                        <a href="<?php echo get_permalink() ?>" class="noimage" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"></a>
                        <?php } ?>
                    <div class="time">
                        <?php echo date('Y-m-d H:i', strtotime($post->post_date)); ?>
                    </div>
                    <p>
    <?php
    echo getFirstWords(strip_tags($post->post_content), 9);
    ?>
                    </p>
                    <!--div class="tags icon_tags">
                        <?php
                        $posttags = get_the_tags();
                        if ($posttags) {
                            foreach ($posttags as $tag) {
                                ?>
                                <div class="tag_<?php echo $tag->slug; ?>"><span><?php echo $tag->name; ?></span></div>
                                <?php
                            }
                        }
                        ?>
                    </div-->
                    
                    
                    
                    <div class="more">
                        <p>
                            <a href="<?php echo get_permalink($similarTag->ID); ?>" class="more">
                                Tovább
                            </a>
                        </p>
                    </div>
                </div>
            <?php
            endwhile;
            wp_reset_query();
            remove_filter('posts_where', 'wpse52070_filter_where');
            ?>
        </div>
        
            <?php if( is_front_page() ) { ?>
                <?php query_posts(array('posts_per_page' => 4, 'cat' => '1438'));
                $count = 0;
                while (have_posts()) : the_post();
                global $post;
                
                if($count == 0) {
                    print('<br /><h2>Műsorelőzetes</h2>');
                }
                $count += 1;
                ?>
                
                <a href="<?php the_permalink(); ?>" class="sidebar-video">
					<span class="img"><?php the_post_thumbnail(array(300,300)); ?></span>
					<span><?php the_title(); ?></span>
				</a>
                <?php
                endwhile;
                wp_reset_query();
            } ?>
        
        <?php } ?>
    </section>
    <br />
	<?php echo do_shortcode("[banner group='gyermek-sidebar']") ?>
	<?php } else { ?>
		<div class="sidebar_nav">
			<a class="internetszuro" href="/kategoria/cikkek/internetszuro">Internetszűrő</a>
			<a class="gyerekzar" href="/kategoria/cikkek/gyerekzar">Gyerekzár</a>
		</div>
		<br />
		<?php include_once 'components/sidebar_slogen.php'; ?>
		<a href="/videok/gyerekzar-1" class="sidebar-video">
    		<span class="img"><img src="<?php bloginfo('template_directory'); ?>/img/video.png" /></span>
    		<span>Gyerekzár</span>
    	</a>
		
		
		<?php if(is_page(15064)) { ?>
			<h2>Videók</h2>
			<?php query_posts(array('posts_per_page' => 3, 'cat' => '391'));
            while (have_posts()) : the_post();
                global $post;
                
                ?>
                <a href="<?php the_permalink(); ?>" class="sidebar-video">
					<span class="img"><?php the_post_thumbnail(array(300,300)); ?></span>
					<span><?php the_title(); ?></span>
				</a>
            <?php
            endwhile;
            wp_reset_query();
		
		} else  { ?>
		
		
		<?php } ?>
	<?php } ?>
</aside>
