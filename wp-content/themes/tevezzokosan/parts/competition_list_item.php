<div class="competition_list_item">
	<?php if ( has_post_thumbnail() ) { ?>
		<a href="<?php echo get_permalink() ?>" class="img" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_post_thumbnail(array(150,150)); ?></a>
	<?php } else { ?>
		<a href="<?php echo get_permalink() ?>" class="noimage" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"></a>
	<?php } ?>
	<?php $title = get_the_title(); ?>
	<h3 class="<?php if(strlen($title = 0)) print("hidden") ?>"><a href="<?php echo get_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
	<?php // the_excerpt_rereloaded('12','Olvass tovább','','div','more','no'); ?>
	<?php // echo get_field('kepalairas');?>
	<?php // echo get_field('leiras');?>
	<h4 class="author"><?php echo get_field('szerzo_neve');?> (<?php echo get_field('szerzo_eletkora');?>)</h4>
</div>
