<?php get_header(); ?>
	<main class="herd front">
                <div class="clear">
                    <?php $offest = date('w', strtotime('now - 1 day')); ?>
                    <?php $recent = new WP_Query("cat=438&offset=".$offest."&posts_per_page=1");
                    $excludeds = array();
                    while ($recent->have_posts()) : $recent->the_post();
                        global $post;
                        $excludeds[] = $post->ID;
                    ?>
                        <article class="kiemelt">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail('main-slider'); ?>
                                <span class="metabox">
		                            <span class="title"><?php the_title(); ?></span>
		                            <span class="text"><?php echo getFirstWords(strip_tags($post->post_content), 25); ?></span>
		                        </span>
                            </a>
                            <!-- p class="more">
                                <a class="morea" href="<?php the_permalink(); ?>" title="Olvass tovább">
                                    Olvass tovább
                                </a>
                            </p -->
                        </article>
                    <?php endwhile; ?>
                </div>
                <div class="clear">
                    
                    <?php
                     $recent = new WP_Query(array("category__in" => array(6,7,3), "showposts" => 3, "post__not_in" => $excludeds));
                    while ($recent->have_posts()) : $recent->the_post();
                        global $post;
                       // print_r($post->ID);
                    ?>
                        <article class="vertical">
                            <a href="<?php echo get_permalink() ?>" class="img" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_post_thumbnail('suggested-thumb3'); ?></a>
                            <h3>
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h3> 
                            <p><?php echo getFirstWords(strip_tags($post->post_content), 15); ?></p>
                            <p class="more">
                                <a class="morea" href="<?php the_permalink(); ?>" title="Olvass tovább">
                                    Olvass tovább
                                </a>
                            </p> 
                        </article>
                    <?php endwhile; ?>
                </div>
		
		<div class="facebook">
				<iframe class="facebook" src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FTevezzOkosan&amp;width=620&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=827066393980533" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:158px;" allowTransparency="true"></iframe>
		</div>
	</main>
	<?php get_sidebar(); ?>
<?php get_footer('parents'); ?>
