<?php get_header(); ?>
	<main class="video-herd">
		<div class="breadcrumbs">
			<?php if(function_exists('bcn_display')) bcn_display(); ?>
		</div>
		<div class="clear">
		<?php if ( have_posts() ) : ?>
			<h2><?php single_cat_title(); ?></h2>
			<div class="clear">
				<?php while ( have_posts() ) : the_post(); ?>
					<article class="video">
						<?php if ( has_post_thumbnail() ) { ?>
							<a href="<?php echo get_permalink() ?>" class="video_img" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_post_thumbnail(array(300,300)); ?></a>
						<?php } else { ?>
							<a href="<?php echo get_permalink() ?>" class="noimage" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"></a>
						<?php } ?>
						<?php $title = get_the_title(); ?>
						<h3 class=""><a href="<?php echo get_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a></h3>
						<?php the_excerpt_rereloaded('11','Nézd meg a videót','','div','more','no'); ?>
					</article>
				<?php endwhile; ?>
			
				<?php if(function_exists('wp_page_numbers')) : wp_page_numbers(); endif; ?>
				<?php else : ?>
			</div>
		<?php endif; ?>
		</div>
	</main>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>
