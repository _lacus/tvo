<?php get_header(); ?>
	<main class="single">
		<div class="breadcrumbs">
			<?php if(function_exists('bcn_display')) bcn_display(); ?>
		</div>
		<?php include_once 'components/suggestions.php'; ?>
	</main>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>
