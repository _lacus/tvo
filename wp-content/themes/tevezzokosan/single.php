<?php get_header(); ?>
	<main class="single <?php if(in_category( array(391,1438) )) { ?>video<?php } ?> <?php if(in_category( array(4, 7, 468, 2, 6, 4, 1426, 3, 1428, 1433, 1432) )) { ?>cikk<?php } ?>">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="breadcrumbs">
				<?php if(function_exists('bcn_display')) bcn_display(); ?>
			</div>
			<h2><?php the_title(); ?></h2>
			<iframe class="facebook_like" src="http://www.facebook.com/plugins/like.php?href=<?php echo urlencode(get_permalink($post->ID)); ?>&amp;layout=standard&amp;show_faces=false&amp;action=like&amp;colorscheme=light" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
			<div class="program_meta">
				<?php
					if(in_category(array(483))) {
						echo get_the_category_list();
						echo "&nbsp;&nbsp;&nbsp;";
					}
				?>
				<?php the_date('Y. m. d. - G:i', '<span class="date">', '</span>'); ?>
			</div>
			<?php if ( has_post_thumbnail() ) { ?>
				<div class="single_image">
					<?php the_post_thumbnail(array(400,400)); ?>
				</div>
			<?php } ?>
			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
			
			<?php if(get_field('szuloknek')) { ?>
				<div class="content_for_parents">
					<hr />
					<div class="tags icon_tags">
						<?php the_tags('',''); ?>
					</div>
					<h4><a name="szuloknek"></a> Információ szülőknek:</h4>
					<?php echo the_field('szuloknek'); ?>
					<a class="info" href="/piktogramokrol-reszletesen">Piktogramokról részletesen</a>
				</div>
			<?php } ?>
		<?php endwhile; endif; ?>
		<?php edit_post_link('Módosítás', '<p>', '</p>'); ?>
	</main>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>
