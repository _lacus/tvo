<?php get_template_part('html-head'); ?>
<div class="topbanner">
	<section class="header-banner desctop">
		<?php echo do_shortcode( "[banner group='gyermek-panoramabanner']" ) ?>
	</section>
	<section class="header-banner mobile">
		<?php echo do_shortcode( "[banner group='gyermek-panoramabanner-mobil']" ) ?>
	</section>
</div>
<div class="middle">
	<?php if(is_front_page() || is_page(12217) || in_category(array(483, 5, 1408, 1375, 1434))) { ?>
	<header role="banner" class="children">
		<h1 class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		<div class="slogan"></div>
		<section class="header-top">
			<!--nav id="social_menu" role="navigation">
				<?php // wp_nav_menu( array( 'theme_location' => 'top', 'menu_class' => 'social-menu' ) ); ?>
			</nav-->
			<div class="search-box">
				<?php get_search_form(); ?>
			</div>
			<a class="switch to_parents" href="/szulok-oldala">Szülőknek</a>
		</section>
		<nav id="main_menu" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
		</nav>
	</header>
	<?php } else { ?>
	<header role="banner" class="parents">
		<h1 class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		<div class="slogan"></div>
		<section class="header-top">
			<nav id="social_menu" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'top', 'menu_class' => 'social-menu' ) ); ?>
			</nav>
			<div class="search-box">
				<?php get_search_form(); ?>
			</div>
			<a class="switch to_children" href="/">Gyerekeknek</a>
		</section>
		<nav id="main_menu" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary-parents', 'menu_class' => 'nav-menu' ) ); ?>
		</nav>
	</header>
	<?php } ?>
