<?php get_header(); global $post;?>
<div id="content" class="flipped">
	<section class="main">
		<div class="breadcrumbs">
			<?php if(function_exists('bcn_display')) bcn_display(); ?>
		</div>
		<!-- h2 class="archive-title"><?php // echo single_cat_title(); ?></h2-->
		<div class="articles">
			<?php
                        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			function filter_where($where = '') {
				$where .= " AND post_date >= '" . date('Y-m-d 23:59:59', strtotime('now')) . "' ";
				return $where;
			}
			add_filter('posts_where', 'filter_where');
			query_posts(array('paged' => $paged, 'posts_per_page' => 10, 'cat'=>5, 'orderby'=>'date','order'=>'DESC', 'order'=>'asc' ));
			while ( have_posts() ) : the_post(); ?>
					<div class="article">
						<div class="date"><?php echo date('Y. m. d. H:i', strtotime($post->post_date)); ?></div>
						<div class="clear"></div>
                                                <?php $helyszin = get_field('helyszin'); if(!empty($helyszin)){ ?>
                                                    <div class="place">
                                                        <?php echo $helyszin; ?>
                                                    </div>
                                                <?php
                                                }
                                                ?>
						<div class="clear"></div>
						<?php if ( has_post_thumbnail() ) { ?>
							<a href="<?php echo get_permalink() ?>" class="img" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_post_thumbnail(array(200,200)); ?></a>
						<?php } else { ?>
							<a href="<?php echo get_permalink() ?>" class="noimage" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"></a>
						<?php } ?>
						<?php $title = get_the_title(); ?>
						<h3 class="<?php if(strlen($title = 0)) print("hidden") ?>"><a href="<?php echo get_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a></h3>
							<?php the_excerpt_rereloaded('12','Tovább','','div','more','no'); ?>
					</div>
			<?php endwhile;
			if(function_exists('wp_page_numbers')) { wp_page_numbers(); }
			wp_reset_query();
			remove_filter('posts_where', 'filter_where');
			?>
			<br />
		</div>
	</section>
	
	<?php get_template_part( "sidebar", "programs" ); ?>
</div>
<?php get_footer(); ?>
