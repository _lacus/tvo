<?php

$napok = array("vasárnap", "hétfő", "kedd", "szerda", "csütörtök", "péntek", "szombat", "vasárnap");
$honap = array('','január','február','március','április','május','junius','julius','augusztus','szeptember','oktober','november','december');

if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'suggested-thumb', 380, 216, true ); //(cropped)
	add_image_size( 'thumb-bigger', 440, 220, true ); //(cropped)
	add_image_size( 'suggested-thumb2', 125, 125, true ); //(cropped)
	add_image_size( 'suggested-thumb3', 150, 150, true ); //(cropped)
	add_image_size( 'main-slider', 630, 350, true ); //(cropped)
	add_image_size( 'suggested-thumb5', 220, 220, true ); //(cropped)
}

/*JQUERY repair*/
if ( !is_admin() ) {
	function changejquery() {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
		wp_enqueue_script( 'jquery' );
	}
	add_action('init', 'changejquery');
}

// Disable theme editor and plugin editor
//define( 'DISALLOW_FILE_EDIT', true );
//define( 'DISALLOW_FILE_MODS', true );

// MENU SETTINGS

// Register Menus
register_nav_menu( 'primary', __( 'Navigation Menu Children', 'leather' ) );
register_nav_menu( 'primary-parents', __( 'Navigation Menu Parents', 'leather' ) );
register_nav_menu( 'top', __( 'Social Menu', 'leather' ) );
register_nav_menu( 'sidebar', __( 'Sidebar menu', 'leather' ) );
register_nav_menu( 'footer', __( 'Footer menu', 'leather' ) );
register_nav_menu( 'lexikon', __( 'Lexikon menu', 'leather' ) );
//register_nav_menu( 'footer-social', __( 'Footer social menu', 'leather' ) );

// CONTENT

/*excrept rereloaded*/
function the_excerpt_rereloaded($words = 40, $link_text = 'Continue reading this entry &#187;', $allowed_tags = '', $container = 'p', $atagclass = 'morea', $smileys = 'no', $need_more_button = true , $return = false) {
	global $post;
	
	if ( $allowed_tags == 'all' ) 
		$allowed_tags = '<a>,<i>,<em>,<b>,<strong>,<ul>,<ol>,<li>,<span>,<blockquote>,<img>';
	
	$text = preg_replace('/\[.*\]/', '', strip_tags($post->post_content, $allowed_tags));
	
	$text = explode(' ', $text);
	$tot = count($text);
	
	for ( $i=0; $i<$words; $i++ ) :
		$output .= $text[$i] . ' ';
	endfor;
		
	if ( $smileys == "yes" ) 
		$output = convert_smilies($output);
	
	$data = '<p>'.force_balance_tags($output);
	
	//ha a szöveg hosszabb, mint amennyi szót meg akarunk jeleníteni, akkor '...', különben lezáró </p>
	if ( $i < $tot ) :
		$data .= '...';
	else :
		$data .= '</p>';
	endif;
	
	//if ( $i < $tot ) : <-- mindig kell a "more"
		if ( $container == 'p' || $container == 'div' ) : $data .= '</p>'; endif;
		if ( $container != 'plain' ) : $data .= '<'.$container.' class="more">';
			if ( $container == 'div' ) : $data .= '<p>'; endif;
		endif;
	
		//$need_more_button alapértelmezetten true, tehát kiírja a gombot is
		if ($need_more_button) { 
			$data .= '<a class="'.$atagclass.'" href="'.get_permalink().'" title="'.$link_text.'">'.$link_text.'</a>';
		}
	
		if ( $container == 'div' ) : $data .= '</p>'; endif;
		if ( $container != 'plain' ) : $data .= '</'.$container.'>'; endif;
		if ( $container == 'plain' || $container == 'span' ) : $data .= '</p>'; endif; 
		//endif;
	
	// $return alapértelmezetten false, tehát kiírja a változó tartalmát
	if ($return)
		return $data;
	else
		echo $data;
}

add_theme_support( 'post-thumbnails' );

function getFuturePostsByCategoryId($categoryId){
    $query = new WP_Query( $args = array(
        'cat' => $categoryId,
        'post_status' => array('future'),
        'orderby' => 'post_date',
        'order' => 'ASC'
        ));
    
    return $query;    
}

function getPostsOnDateByCategoryId($timestamp, $categoryId){
    //parent 485 type
    //parent 486 channel
    $query = new WP_Query( $args = array(
        'date_query' => array(
                                'year' => date('Y', $timestamp),
                                'month' => date('m', $timestamp),
                                'day'   => date('d', $timestamp)
                             ),
        'cat' => $categoryId,
        'post_status' => array('publish', 'future'),
        'orderby' => 'post_date',
        'order' => 'ASC'
        ));
    
    $sameDateTimeChecker = array();
    
    while($query->have_posts()){
        $query->next_post();        
        $post_categories = wp_get_post_categories($query->post->ID);
        foreach($post_categories as $c){
                $cat = get_category( $c );
                if($cat->parent === 485){
                    $query->post->customCategory = $cat->name;
                }
                if($cat->parent === 486){
                    $query->post->channel = $cat->name;
                }
        }
        $sameDateTimeChecker[] = $query->post->post_date . $query->post->post_title;
        
        
    }
    
    
    return $query;
}

function getFirstAndLastPostDateInCategory($categoryId){
    
    $intervalArray = array();
    
    $query = new WP_Query( $args = array(
        'cat' => $categoryId,
        'post_status' => array('publish', 'future'),
        'orderby' => 'post_date',
        'order' => 'ASC',
        'posts_per_page' => 1
        ));
    
    
    while($query->have_posts()){
        $intervalArray['firstDate'] = $query->post->post_date;
        $query->next_post();
    }
    
    $query2 = new WP_Query( $args = array(
        'cat' => $categoryId,
        'post_status' => array('publish', 'future'),
        'orderby' => 'post_date',
        'order' => 'DESC',
        'posts_per_page' => 1
        ));
    
    
    while($query2->have_posts()){
        $intervalArray['lastDate'] = $query2->post->post_date;
        $query2->next_post();
    }
    
    return $intervalArray;
    
}

function getSuggestionsUrl($diff){
    $actual_link = get_permalink(12217);
    
    $baseDate = date('Y-m-d', strtotime('now'));
    
    if(empty($_GET['datum'])){
        $generatedDate = date('Y-m-d', strtotime($baseDate . $diff));
    }else{
        $baseDate = $_GET['datum'];
        $generatedDate = date('Y-m-d', strtotime($baseDate . $diff));
    }
    
    $generatedUrl = $actual_link . '?datum=' . $generatedDate;
    return $generatedUrl;
}


$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

if(!empty($_GET['paged']))
    $paged = $_GET['paged'];



function getDaysOfThisWeek($date = null){
    $daysOfThisWeek = array();
    $currentDate = strtotime('now');
    if($date != null){
        $currentDate = strtotime($date);
    }
    for($i = 0; $i <= 6; $i++){
        $daysOfThisWeek[] = strtotime($date . ' + ' . $i . ' days', $currentDate);
    }
    return $daysOfThisWeek;
}

function getFirstWords($sentence, $numberOfWords){
    setlocale(LC_ALL, 'hu_HU');
    $numberOfWords -= 2;
    $shortened = implode(' ', array_slice(explode(' ', $sentence), 0, $numberOfWords));
    
    $lastChar = substr($shortened, strlen($shortened) - 1, 1);
    
    if(!ctype_alpha($lastChar)){
        $shortened = substr($shortened, 0, -1);
    }
    
    $shortened .= '...';
    
    return $shortened;
}
