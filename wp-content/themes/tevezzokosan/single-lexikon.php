<?php get_header(); ?>
	<main class="single <?php if(in_category( array(391,1438) )) { ?>video<?php } ?> <?php if(in_category( array(4, 7, 468, 2, 6, 4, 1426, 3, 1428, 1433, 1432) )) { ?>cikk<?php } ?>">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="breadcrumbs">
				<?php if(function_exists('bcn_display')) bcn_display(); ?>
			</div>
			<h2>Lexikon - <?php the_title(); ?></h2>
			<?php wp_nav_menu( array( 'theme_location' => 'lexikon', 'menu_class' => 'content-menu' ) ); ?>
			<?php if ( has_post_thumbnail() ) { ?>
				<div class="single_image">
					<?php the_post_thumbnail(array(400,400)); ?>
				</div>
			<?php } ?>
			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
		<?php endwhile; endif; ?>
		<?php edit_post_link('Módosítás', '<p>', '</p>'); ?>
	</main>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>
