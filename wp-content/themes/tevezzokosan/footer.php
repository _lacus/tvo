		
		<footer id="footer" role="contentinfo">
			<nav id="footer_menu" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'nav-menu' ) ); ?>
				<p class="copy_text">© TévézzOkosan <?php echo date("Y"); ?></p>
			</nav>
			<nav id="supporter">
				<ul>
					<li>
						<a target="_blank" href="http://www.kabelszov.hu">
						A weboldal készült a<br />
						<img width="145" height="42" alt="mksz logo" src="<?php echo get_template_directory_uri(); ?>/images/mksz_logo.png"><br />
						megbízásából.
						</a>
					</li>
					<li>
						<a target="_blank" href="http://nmhh.hu/">
							<img border="0" title="NMHH" alt="NMHH" style="height: 60px;" src="<?php echo get_template_directory_uri(); ?>/images/01_nmhh_logo.jpg">
						</a>
					</li>
					<li>
						<a target="_blank" href="http://mta.hu/cikkek/kozoktatasi-elnoki-bizottsag-127631">
							<img border="0" title="MTA Közoktatási Elnöki Bizottság" alt="MTA Közoktatási Elnöki Bizottság" style="height: 60px;" src="<?php echo get_template_directory_uri(); ?>/images/02_mta_logo.jpg">
						</a>
					</li>
					<li>
						<a target="_blank" href="http://www.mipszi.hu/">
							<img border="0" title="MIPSZI" alt="MIPSZI" style="height: 60px;" src="<?php echo get_template_directory_uri(); ?>/images/03_mipszi_logo.jpg">
						</a>
					</li>
					<li>
						<a target="_blank" href="http://www.otthonaneten.hu.hu/">
							<img border="0" title="Otthon a neten" alt="Otthon a neten" style="height: 82px;" src="<?php echo get_template_directory_uri(); ?>/images/onet.png">
						</a>
					</li>
					
					<li>
						<a target="_blank" href="http://unicef.hu/">
							<img border="0" title="UNICEF" alt="UNICEF" style="height: 100px;" src="<?php echo get_template_directory_uri(); ?>/images/unicef.png">
						</a>
					</li>
				</ul>
			</nav>
			
			
			
		</footer>
	</div>
<?php wp_footer(); ?>
</body>
</html>
